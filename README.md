# Docs for PHP 8 Version

## Requirements

* Laravel ^8.0
* PHP ^8.0

## Install

First install package with composer:

    composer require eurofirany/allegro-connector

Migrate tables for queue
    
    php artisan migrate

Next public config file to your app config folder:

    php artisan vendor:publish --tag=allegro_config

This generates **ac.php** file in **config** folder

    'token_refresh_frequency' => '0 */2 * * *', // CRON for automatic refresh allegro tokens

    /** Queue Settings */
    'queue' => [
        'enabled' => false, // Is queue for connector enabled
        'max_tries' => 3, // Max tries for failed job
        'max_running_jobs' => 5, // Max running jobs at the same time
        'max_wait_seconds' => 300, // Wait seconds when there are too many jobs
        'sleep_seconds' => 10 // Seconds to wait between attempts
    ]


## Lumen Framework

Put this lines in **bootstrap/app.php** file:

    $app->register(\Eurofirany\AllegroConnector\Providers\AllegroConnectorServiceProvider::class);
    $app->register(\Eurofirany\Microservices\Providers\MicroservicesConnectorServiceProvider::class);

    $app->configure('allegro');

For publish vendor config install **laravelista/lumen-vendor-publish** package
or just create **allegro.php** file in **config** folder.

## Commands

name|description
---|---
|allegro:account:create|Create new Allegro Account in DB
|allegro:account:update {account_id?}|Update account by give in or if empty select form list
|allegro:account:delete {account_id?}|Delete account by give in or if empty select form list