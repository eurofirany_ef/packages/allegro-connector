<?php

namespace Eurofirany\AllegroConnector;

use Eurofirany\ConnectorsQueue\ConnectorsQueue;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class Api
{
    const AUTHORIZE_URL = 'https://allegro.pl';
    const API_URL = 'https://api.allegro.pl';

    private Client $api;
    private ConnectorsQueue $connectorsQueue;

    private string $baseUri;
    private array $parameters = [];
    private string $clientId;
    private string $secret;
    private string $token = '';
    private string $deviceCode;

    private string $query = '';

    /**
     * Api constructor.
     */
    public function __construct()
    {
        // Create Client
        $this->api = new Client();

        // Set baseUri
        $this->baseUri = self::API_URL;

        $this->connectorsQueue = new ConnectorsQueue();

        $this->connectorsQueue->setting(
            config('allegro.queue.enabled', false),
            config('allegro.queue.max_tries', 3),
            config('allegro.queue.max_running_jobs', 5),
            config('allegro.queue.max_wait_seconds', 300),
            config('allegro.queue.sleep_seconds', 10),
            config('allegro.queue.max_per_minute', 0)
        );
    }

    /**
     * @throws GuzzleException
     * @throws Exception
     */
    public function request(string $method, string $uri, int $ttl = 36000): mixed
    {
        return $this->connectorsQueue->call(function () use ($method, $uri, $ttl) {
            $key = base64_encode($this->clientId . $method . $uri . serialize($this->parameters));

            $uri .= $this->query;

            if ($ttl == 0)
                return json_decode(
                    ($this->api->request($method, $this->baseUri . '/' . $uri, $this->parameters))->getBody()->__toString(),
                    1
                );

            // Get response
            $response = Cache::remember($key, $ttl, function () use ($method, $uri) {
                return json_decode(
                    ($this->api->request($method, $this->baseUri . '/' . $uri, $this->parameters))->getBody()->__toString(),
                    1
                );
            });

            // Reset parameters
            $this->resetParameters();

            return $response;
        });
    }

    /**
     * Construct headers
     * @param string $authorization
     */
    public function setHeader(string $authorization = 'Bearer')
    {
        // Set authorization
        switch ($authorization) {
            case 'Bearer';
                $this->parameters['headers']['Authorization'] = 'Bearer ' . $this->token;
                break;
            case 'Basic';
                $this->parameters['headers']['Authorization'] = 'Basic ' . base64_encode($this->clientId . ':' . $this->secret);
                break;
        }
    }

    /**
     * Set content type into parameters
     * @param string $type
     */
    public function setContentType(string $type = 'application/vnd.allegro.public.v1+json')
    {
        $this->parameters['headers']['Content-type'] = $type;
    }

    /**
     * @param string $accept
     */
    public function setAccept(string $accept = 'application/vnd.allegro.public.v1+json')
    {
        $this->parameters['headers']['Accept'] = $accept;
    }

    /**
     * @param $key
     * @param $value
     */
    public function setFormParam($key, $value)
    {
        // Insert form params
        $this->parameters['form_params'][$key] = $value;
    }

    /**
     * @param string $base
     */
    public function setBAseUri(string $base = 'api')
    {
        // Set authorization
        $this->baseUri = match ($base) {
            'api' => self::API_URL,
            'auth' => self::AUTHORIZE_URL,
        };
    }

    /**
     * @param $offset
     */
    public function setOffset($offset)
    {
        $this->setFormParam('offset', $offset);
    }

    /**
     * @param int $limit
     */
    public function setLimit(int $limit)
    {
        if ($limit)
            $this->setFormParam('limit', $limit);

    }

    /**
     * @param $userId
     */
    public function setUserId($userId)
    {
        $this->setFormParam('user.id', $userId);
    }

    /**
     * @param $client_id
     */
    public function setClientId($client_id)
    {
        $this->clientId = $client_id;
    }

    /**
     * @param $secret
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;
    }

    /**
     *
     */
    public function addClientId()
    {
        $this->setFormParam('client_id', $this->clientId);
    }

    /**
     * @return string
     */
    public function getClientId(): string
    {
        return $this->clientId;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getDeviceCode(): string
    {
        return $this->deviceCode;
    }

    /**
     * @param string $deviceCode
     */
    public function setDeviceCode(string $deviceCode)
    {
        $this->deviceCode = $deviceCode;
    }

    /**
     * @param string|int $key
     * @param $value
     */
    public function setBodyJson(string|int $key, mixed $value)
    {
        $this->parameters['json'][$key] = $value;
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     *
     */
    public function resetParameters()
    {
        $this->parameters = [];
        $this->query = '';
    }

    /**
     * @param string $key
     * @param string $value
     */
    public function addQueryParam(string $key, string $value): void
    {
        $this->query .= Str::startsWith($this->query, '?') ?
            sprintf("&%s=%s", $key, $value) :
            sprintf("?%s=%s", $key, $value);
    }

    /**
     * Set if you want or not to ignore queue for next requests
     * @param bool $ignoreQueueForRequests
     */
    public function setIgnoreQueueForRequests(bool $ignoreQueueForRequests)
    {
        $this->connectorsQueue->setWithQueue(!$ignoreQueueForRequests);
    }
}
