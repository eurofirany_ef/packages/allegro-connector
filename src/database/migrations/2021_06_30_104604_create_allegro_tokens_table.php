<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAllegroTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('allegro_tokens'))
            Schema::create('allegro_tokens', function (Blueprint $table) {
                $table->id();
                $table->string('allegro_account_id');
                $table->string('device_code');
                $table->text('token');
                $table->text('refresh_token');
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('allegro_tokens');
    }
}
