<?php

namespace Eurofirany\AllegroConnector\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class AllegroToken
 * @property int allegro_account_id
 * @property string token
 * @property string device_code
 * @property string refresh_token
 * @property AllegroAccount allegroAccount
 * @mixin Builder
 */
class AllegroToken extends Model
{
    use HasFactory;

    protected $fillable = ['allegro_account_id', 'device_code', 'token', 'refresh_token'];

    /**
     * Return allegro account assigned to this token
     * @return BelongsTo
     */
    public function allegroAccount(): BelongsTo
    {
        return $this->belongsTo(AllegroAccount::class);
    }
}
