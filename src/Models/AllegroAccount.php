<?php

namespace Eurofirany\AllegroConnector\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class AllegroAccount
 *
 * @mixin Builder
 * @property int id
 * @property string login
 * @property int client_id
 * @property string secret
 * @property AllegroToken token
 */
class AllegroAccount extends Model
{
    use HasFactory;

    protected $fillable = ['login', 'client_id', 'secret'];

    /**
     * Return token for Allegro Account
     * @return HasOne
     */
    public function token(): HasOne
    {
        return $this->hasOne(AllegroToken::class);
    }

    /**
     * Check if account is currently authorized
     * @return bool
     */
    public function isAuthorized(): bool
    {
        return $this->token()->first() !== null;
    }
}
