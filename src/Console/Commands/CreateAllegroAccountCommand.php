<?php

namespace Eurofirany\AllegroConnector\Console\Commands;

class CreateAllegroAccountCommand extends AllegroAccountDataCommand
{
    protected $signature = 'allegro:account:create';
    protected $description = 'Create new allegro account';

    public function handle()
    {
        $this->getDataFromUser();

        $this->allegroAccountRepository->store([
           'login' => $this->login,
           'client_id' => $this->clientId,
           'secret' => $this->secret
        ]);
    }
}
