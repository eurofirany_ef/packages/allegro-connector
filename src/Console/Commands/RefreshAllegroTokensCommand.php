<?php

namespace Eurofirany\AllegroConnector\Console\Commands;

use Eurofirany\AllegroConnector\Repository\AllegroAccountRepository;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;
use Eurofirany\AllegroConnector\Service as AllegroService;

class RefreshAllegroTokensCommand extends Command
{
    protected $signature = 'refresh:allegro:tokens';

    protected $description = 'Refresh all allegro accounts authorization tokens';
    private AllegroAccountRepository $allegroAccountRepository;


    public function __construct(
        AllegroAccountRepository $allegroAccountRepository
    )
    {
        $this->allegroAccountRepository = $allegroAccountRepository;

        parent::__construct();
    }

    /**
     * @throws GuzzleException
     */
    public function handle()
    {
        foreach($this->allegroAccountRepository->index() as $allegroAccount){
            $allegroService = new AllegroService($allegroAccount);
            if($allegroService->authorizeOrRefreshToken())
                $this->info(sprintf("%s authorized", $allegroAccount->login));
            else
                $this->info(sprintf("%s not authorized", $allegroAccount->login));
        }
    }
}
