<?php

namespace Eurofirany\AllegroConnector\Console\Commands;

class UpdateAllegroAccountCommand extends AllegroAccountDataCommand
{
    protected $signature = 'allegro:account:update {account_id?}';

    protected $description = 'Update allegro account data';

    public function handle()
    {
        $this->getDataFromUser(true);

        $this->allegroAccountRepository->update($this->account, [
            'login' => $this->login,
            'client_id' => $this->clientId,
            'secret' => $this->secret
        ]);
    }
}
