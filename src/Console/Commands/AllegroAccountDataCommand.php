<?php

namespace Eurofirany\AllegroConnector\Console\Commands;

use Eurofirany\AllegroConnector\Models\AllegroAccount;
use Eurofirany\AllegroConnector\Repository\AllegroAccountRepository;
use Illuminate\Console\Command;

class AllegroAccountDataCommand extends Command
{
    protected AllegroAccountRepository $allegroAccountRepository;

    protected string $login;
    protected string|int $clientId;
    protected string $secret;
    protected null|AllegroAccount $account = null;

    public function __construct(AllegroAccountRepository $allegroAccountRepository)
    {
        parent::__construct();
        $this->allegroAccountRepository = $allegroAccountRepository;
    }

    protected function getDataFromUser(bool $withAccount = false)
    {
        if ($withAccount)
            $this->getAccount();

        $this->login = $this->askForParameter('login');
        $this->clientId = $this->askForParameter('client_id');
        $this->secret = $this->askForParameter('secret');
    }

    protected function getAccount()
    {
        $accountId = $this->argument('account_id');

        if ($accountId === null) {
            $options = $this->allegroAccountRepository->index()
                ->map(fn(AllegroAccount $allegroAccount) => sprintf(
                    "%s: %s",
                    $allegroAccount->id,
                    $allegroAccount->login
                ));

            if($options->count() > 0) {
                $option = $this->choice(
                    'Select allegro account: ',
                    $options->toArray()
                );

                $accountId = current(explode(':', $option));
            } else {
                $this->info('No accounts found, create once first');
                die();
            }
        }

        $this->account = $this->allegroAccountRepository->find($accountId);
    }

    private function askForParameter(string $parameter)
    {
        return $this->ask(
            $this->text($parameter),
            $this->account?->{$parameter}
        );
    }

    private function text(string $parameter): string
    {
        return sprintf(
            'Account %s%s:',
            $parameter,
            $this->account ? sprintf(" (%s)", $this->account->{$parameter}) : ''
        );
    }
}