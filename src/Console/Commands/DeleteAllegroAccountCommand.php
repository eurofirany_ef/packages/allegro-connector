<?php

namespace Eurofirany\AllegroConnector\Console\Commands;

use Eurofirany\AllegroConnector\Console\Commands\AllegroAccountDataCommand;
use Exception;

class DeleteAllegroAccountCommand extends AllegroAccountDataCommand
{
    protected $signature = 'allegro:account:delete {account_id?}';

    protected $description = 'Delete allegro account';

    /**
     * @throws Exception
     */
    public function handle()
    {
        $this->getAccount();

        $this->allegroAccountRepository->delete($this->account);
    }
}
