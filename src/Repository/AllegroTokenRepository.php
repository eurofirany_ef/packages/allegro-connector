<?php

namespace Eurofirany\AllegroConnector\Repository;

use Eurofirany\AllegroConnector\Models\AllegroAccount;
use Eurofirany\AllegroConnector\Models\AllegroToken;

/**
 * Class AllegroTokenRepository
 * @property AllegroToken model
 */
class AllegroTokenRepository
{
    protected AllegroAccountRepository $allegroAccountRepository;

    /**
     * Init allegro account
     */
    public function __construct()
    {
        $this->model = new (config('allegro.token_model_class', AllegroToken::class))();

        $this->allegroAccountRepository = new AllegroAccountRepository();
    }

    /**
     * Update or create token for account. Returns AllegroAccount for which token was created.
     * @param int $accountId Account ID for which token will be created
     * @param string $token Token value
     * @param string $refreshToken Refresh token value
     * @param string|null $deviceCode Device code value
     * @return AllegroAccount|null
     */
    public function updateOrCreateToken(
        int $accountId,
        string $token,
        string $refreshToken,
        ?string $deviceCode = null
    ): ?AllegroAccount
    {
        $data = [
            'token' => $token,
            'refresh_token' => $refreshToken
        ];

        if ($deviceCode)
            $data['device_code'] = $deviceCode;

        // Update token
        $this->model->updateOrCreate(
            ['allegro_account_id' => $accountId],
            $data
        );

        // Return updated account
        return $this->allegroAccountRepository->find($accountId);
    }
}