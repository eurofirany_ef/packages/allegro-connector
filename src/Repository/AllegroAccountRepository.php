<?php

namespace Eurofirany\AllegroConnector\Repository;

use Eurofirany\AllegroConnector\Models\AllegroAccount;
use Exception;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class AllegroAccountRepository
 * @property AllegroAccount model
 */
class AllegroAccountRepository
{
    /**
     * Init repository
     */
    public function __construct()
    {
        $this->model = new (config('allegro.model_class', AllegroAccount::class))();
    }

    /**
     * Return all Allegro Accounts
     * @return AllegroAccount[]|Collection
     */
    public function index(): Collection|array
    {
        return $this->model->all();
    }

    /**
     * Store new allegro account
     * @param array $data Data for account to create
     * @return AllegroAccount
     */
    public function store(array $data): AllegroAccount
    {
        return $this->model->create($data);
    }

    /**
     * Update allegro account data
     * @param AllegroAccount $allegroAccount Allegro Account to be updated
     * @param array $data Allegro account data to update
     * @return bool
     */
    public function update(AllegroAccount $allegroAccount, array $data): bool
    {
        return $allegroAccount->update($data);
    }

    /**
     * Delete Allegro Account
     * @param AllegroAccount $allegroAccount Allegro Account to be deleted
     * @return bool|null
     * @throws Exception
     */
    public function delete(AllegroAccount $allegroAccount): ?bool
    {
        return $allegroAccount->delete();
    }

    /**
     * Return Allegro Account by ID, if not exists returns null
     * @param int|string $id ID of account to be returned
     * @return AllegroAccount|null
     */
    public function find(int|string $id): ?AllegroAccount
    {
        return $this->model->find($id);
    }
}
