<?php

namespace Eurofirany\AllegroConnector\Providers;

use Eurofirany\AllegroConnector\Console\Commands\CreateAllegroAccountCommand;
use Eurofirany\AllegroConnector\Console\Commands\DeleteAllegroAccountCommand;
use Eurofirany\AllegroConnector\Console\Commands\RefreshAllegroTokensCommand;
use Eurofirany\AllegroConnector\Console\Commands\UpdateAllegroAccountCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\ServiceProvider;

class AllegroConnectorServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/allegro.php', 'allegro_config');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');

        $this->publishes([
            __DIR__.'/../config/allegro.php' => app()->configPath('allegro.php'),
        ], 'allegro_config');

        if ($this->app->runningInConsole()) {
            $this->commands([
                CreateAllegroAccountCommand::class,
                UpdateAllegroAccountCommand::class,
                DeleteAllegroAccountCommand::class,
                RefreshAllegroTokensCommand::class
            ]);
        }

        $this->callAfterResolving(Schedule::class, function (Schedule $schedule) {
            $schedule->command(RefreshAllegroTokensCommand::class)->cron(
                config('allegro.token_refresh_frequency', '0 */2 * * *')
            );
        });
    }
}
