<?php

namespace Eurofirany\AllegroConnector;

use Eurofirany\AllegroConnector\Models\AllegroAccount;
use Eurofirany\AllegroConnector\Repository\AllegroTokenRepository;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Collection;
use JetBrains\PhpStorm\Pure;

class Service
{
    private Connector $connector;

    private AllegroTokenRepository $allegroTokenRepository;
    private AllegroAccount $allegroAccount;
    private mixed $me;

    /**
     * Service constructor.
     * @param AllegroAccount $allegroAccount Allegro Account for which connector is initialized
     */
    public function __construct(AllegroAccount $allegroAccount)
    {
        $this->allegroTokenRepository = new AllegroTokenRepository();

        // Create connector
        $this->connector = new Connector($allegroAccount);

        // Allegro account
        $this->allegroAccount = $allegroAccount;
    }

    /**
     * Check if we are authorized to make request and keep internal user data
     * @throws GuzzleException
     */
    public function checkAuthorization(): bool
    {
        // Check if we need to authorize by
        try {
            $this->me = $this->getMe();
        } catch (ClientException $clientException) {
            // We do need to authorise
            if ($clientException->getCode() == 401)
                return false;
        }

        return true;
    }

    /**
     * Get user data from Allegro
     * @return mixed
     * @throws GuzzleException
     */
    public function getMe(): mixed
    {
        return $this->connector->getMe();
    }

    /**
     * Authorize User, get his access codes
     * @throws GuzzleException
     */
    public function getAuthorizationCodes()
    {
        // Get access data
        return $this->connector->getAccessCodes();
    }

    /**
     * Update account token
     * @param string $deviceCode Device code for which access will be updated
     * @throws GuzzleException
     */
    public function updateToken(string $deviceCode)
    {
        // Get new token data
        $tokenData = $this->connector->getToken($deviceCode);

        // Update token in database
        $allegroAccount = $this->allegroTokenRepository->updateOrCreateToken(
            $this->allegroAccount->id,
            $tokenData['access_token'],
            $tokenData['refresh_token'],
            $deviceCode
        );

        // Update token in connector
        $this->connector->setTokenData($allegroAccount);
    }

    /**
     * Refresh token for account
     * @throws GuzzleException
     */
    public function refreshToken()
    {
        // Get new tokens
        $tokenData = $this->connector->getRefreshedToken($this->allegroAccount->token->refresh_token);

        // Insert new tokens
        $this->allegroTokenRepository->updateOrCreateToken(
            $this->allegroAccount->id,
            $tokenData['access_token'],
            $tokenData['refresh_token']
        );

    }

    /**
     * Calculate pages quantity
     * @param int $total total count of items
     * @return float
     */
    #[Pure] private function calculatePages($total): float
    {
        return ceil($total / $this->connector->getLimit());
    }

    /**
     * Get all active account offers
     * @param int $testMode Is test mode, if true receive only few pages
     * @return array
     * @throws GuzzleException
     */
    public function getAllActiveOffers(int $testMode = 0): array
    {
        // Get only active offers
        $this->connector->addParameter('publication.status', 'ACTIVE');

        return $this->getOffers($testMode);
    }

    /**
     * Get all offers from allegro account, active or not
     * @param int $testMode Is it test mode, if true receive only few pages
     * @return array
     * @throws GuzzleException
     */
    public function getAllOffers(int $testMode = 0): array
    {
        // Set parameters for all auction types
        $this->connector->addParameter('publication.status', [
            'ACTIVE', 'INACTIVE', 'ACTIVATING', 'ENDED'
        ]);

        return $this->getOffers($testMode);
    }

    /**
     * Get all offers with their details from allegro account, active or not
     * @param int $testMode Is it test mode, if true receive only few pages
     * @return array
     * @throws GuzzleException
     */
    public function getAllOffersWithDetails(int $testMode = 0): array
    {
        foreach ($this->getAllOffers($testMode) as $offer)
            $offers[] = $this->getOfferDetails($offer['id']);

        return $offers ?? [];
    }

    /**
     * Get all active offers with their details from allegro account, active or not
     * @param int $testMode Is it test mode, if true receive only few pages
     * @return array
     * @throws GuzzleException
     */
    public function getAllActiveOffersWithDetails(int $testMode = 0): array
    {
        foreach ($this->getAllActiveOffers($testMode) as $offer)
            $offers[] = $this->getOfferDetails($offer['id']);

        return $offers ?? [];
    }

    /**
     * Trigger get offers
     * @throws GuzzleException
     */
    public function getOffers(int $testMode = 0): array
    {
        // Init detailed variants array
        $allOffersWithDetails = [];

        // Get meta data to calculate entities quantity
        $metaData = $this->connector->getOffers(1, 1);

        // Go for all off them
        for ($i = 1; $i <= $this->calculatePages($metaData['totalCount']) + 1; $i++) {
            // Get variants
            $offers = $this->connector->getOffers($i);

            // Get variant details
            foreach ($offers['offers'] as $offer)
                $allOffersWithDetails[$offer['id']] = $offer;

            if ($testMode && $i == 5)
                return $allOffersWithDetails;
            //return array_slice($allOffersWithDetails, 0, 3);
        }

        // Return data
        return $allOffersWithDetails;
    }

    /**
     * Get offer details
     * @param int|string $offerId Offer id for which details will be taken
     * @param int $ttl Data in cache exist duration time
     * @return mixed
     * @throws GuzzleException
     */
    public function getOfferDetails(int|string $offerId, int $ttl = 3360): mixed
    {
        return $this->connector->getOffer($offerId, $ttl);
    }

    /**
     * Get offer variant data
     * @param int $variantId ID of variant to be taken
     * @return mixed
     * @throws GuzzleException
     */
    public function getVariant($variantId): mixed
    {
        return $this->connector->getVariant($variantId);
    }

    /**
     * Get all possible offer variant for account
     * @param int $page Current connector page
     * @return array
     * @throws GuzzleException
     */
    public function getAllVariants(int $page = 1): array
    {
        // Init detailed variants array
        $allVariantsWithDetails = [];
        $userId = $this->getMe()['id'];

        // Get all variants
        while ($variants = $this->connector->getVariants($userId, $page++)) {
            // There are no results? Than let's finish
            if (!count($variants['offerVariants']))
                break;

            foreach ($variants['offerVariants'] as $variant)
                $allVariantsWithDetails[] = $this->connector->getVariant($variant['id']);
        }

        // Return data
        return $allVariantsWithDetails;
    }

    /**
     * Get categories with details and parameters
     * @param array $categoriesId Array of categories id to be taken
     * @return array
     * @throws GuzzleException
     */
    public function getCategories(array $categoriesId = []): array
    {
        foreach ($categoriesId as $categoryId) {
            // Get category details and parameters
            $categories[$categoryId] = $this->getCategory($categoryId) + $this->getCategoryParameters($categoryId);
        }

        return $categories ?? [];
    }

    /**
     * Get all categories data
     * @return mixed
     * @throws GuzzleException
     */
    public function getAllCategories(): mixed
    {
        return $this->connector->getCategories();
    }

    /**
     * Get category details
     * @param int|string $categoryId Category ID for which details will be taken
     * @return mixed
     * @throws GuzzleException
     */
    public function getCategory(int|string $categoryId): mixed
    {
        return $this->connector->getCategory($categoryId);
    }


    /**
     * Get all possible category parameters
     * @param int|string $categoryId Category ID for which parameters will be taken
     * @return mixed
     * @throws GuzzleException
     */
    public function getCategoryParameters(int|string $categoryId): mixed
    {
        return $this->connector->getCategoryParameters($categoryId);
    }

    /**
     * Create Offer variant
     * @param string $name Name of variant
     * @param array $offers Offers applied into variant
     * @param string $parameter Parameter for which variant will be created
     * @param string|null $bindingParameter Additional parameter for variant
     * @return mixed
     * @throws GuzzleException
     */
    public function createVariant(
        string  $name,
        array   $offers = [],
        string  $parameter = 'color/pattern',
        ?string $bindingParameter = null
    ): mixed
    {
        return $this->connector->createVariant(
            $name,
            $this->parseProductsForVariants($offers),
            $parameter,
            $bindingParameter
        );
    }

    /**
     * Parse products for variant generation and remove duplicates
     * @param array|Collection $auctions Auction prepared variants
     * @return array
     */
    private function parseProductsForVariants(array|Collection $auctions): array
    {
        $output = [];
        $i = 1;
        foreach ($auctions as $auction) {
            // Generate key
            $key = $auction['external']['id'] ?? $auction['ean'];
            $key = 'AUTO_' . filter_var($key, FILTER_SANITIZE_NUMBER_INT);

            if (isset($output[$key]))
                $key = $key . '_' . $i++;

            $output[$key] = [
                'id' => $auction['id'],
                'colorPattern' => $key
            ];
        }

        // Reset keys
        sort($output);

        return $output;
    }

    /**
     * Remove variant from allegro account
     * @param array $variant Variant to be removed
     * @throws GuzzleException
     */
    public function removeVariant(array $variant)
    {
        $this->connector->removeVariant($variant['id']);
    }

    /**
     * Get all sets from allegro
     * @return array
     * @throws GuzzleException
     */
    public function getSets(): array
    {
        // Init detailed variants array
        $allSetsWithDetails = [];

        // Get meta data to calculate entities quantity
        $metaData = $this->connector->getSets($this->getMe()['id'], 0, 1);

        // Go for all off them
        for ($i = 0; $i <= $this->calculatePages($metaData['totalCount']); $i++) {

            // Get variants
            $sets = $this->connector->getSets($this->getMe()['id'], $i);

            // Get variant details
            foreach ($sets['promotions'] as $set)
                $allSetsWithDetails[$set['id']] = $set;
        }

        // Return data
        return $allSetsWithDetails;
    }

    /**
     * Remove set from allegro account
     * @param string|int $setId Set ID to be removed
     */
    public function removeSet(string|int $setId)
    {
        $this->connector->removeSet($setId);
    }

    /**
     * Generate bundle sets
     * @param array $offerIds Offer ids to be in bundle
     * @param int $discount Discount on bundle
     * @throws GuzzleException
     */
    public function generateBundleSet(array $offerIds = [], int $discount = 0)
    {
        // Parse offer for bundle
        foreach ($offerIds as $offer) {
            $offers[$offer] = [
                'id' => $offer,
                'quantity' => !isset($offers[$offer]) ? 1 : $offers[$offer]['quantity'] + 1,
                'promotionEntryPoint' => !isset($offers)
            ];
        }

        // Get rid of array keys to match allegro requirements
        $offers = array_values($offers ?? []);

        $this->connector->generateSets('ORDER_FIXED_DISCOUNT', !empty($offers) ? $offers : [], $discount);
    }

    /**
     * Get orders between to dates
     * @param int $ordersFrom Timestamp to get from
     * @param int $ordersTo Timestamp to get to
     * @param string $status Status from which orders will be get
     * @return array
     * @throws GuzzleException
     */
    public function getOrdersBetween(
        int    $ordersFrom,
        int    $ordersTo,
        string $status = 'READY_FOR_PROCESSING'
    ): array
    {
        $currentOffset = 0;
        $allOrders = [];

        while (true) {
            $orders = $this->getOrders(['dateFrom' => $ordersFrom, 'dateTo' => $ordersTo], $status, $currentOffset);

            if (count($orders) > 0) {
                $allOrders = array_merge($allOrders, $orders);
                $currentOffset += 100;
            } else
                break;
        }

        return $allOrders;
    }

    /**
     * Get orders by parameters
     * @param array $params Parameters for search
     * @param string|null $status Status for which orders will be taken
     * @param int $offset Offset for getter
     * @return mixed
     * @throws GuzzleException
     */
    public function getOrders(
        array   $params = [],
        ?string $status = 'READY_FOR_PROCESSING',
        int     $offset = 0
    ): mixed
    {
        if (isset($params['dateFrom']) || isset($params['dateTo'])) {
            if (isset($params['dateFrom']))
                $params['updatedAt.gte'] = date("Y-m-d\TH:i:s\Z", $params['dateFrom']);

            if (isset($params['dateTo']))
                $params['updatedAt.let'] = date("Y-m-d\TH:i:s\Z", $params['dateTo']);

            unset($params['dateFrom']);
            unset($params['dateTo']);
        }

        return $this->connector->getOrders($params, $status, $offset)['checkoutForms'];
    }

    /**
     * Get refunds from allegro between to dates
     * @param int $dateFrom Timestamp to get from
     * @param int $dateTo Timestamp to get to
     * @return array
     * @throws GuzzleException
     */
    public function getRefundsBetween(int $dateFrom, int $dateTo): array
    {
        $currentOffset = 0;
        $allRefunds = [];

        while (true) {
            $refunds = $this->getRefunds($dateFrom, $dateTo, $currentOffset);

            if (count($refunds) > 0) {
                $allRefunds = array_merge($allRefunds, $refunds);
                $currentOffset += 100;
            } else
                break;
        }

        return $allRefunds;
    }

    /**
     * Get offset between two dates with offset
     * @param int $dateFrom Timestamp to get from
     * @param int $dateTo Timestamp to get to
     * @param int $offset Offset for getter
     * @return mixed
     * @throws GuzzleException
     */
    public function getRefunds(int $dateFrom, int $dateTo, int $offset = 0): mixed
    {
        $timeFrom = date("Y-m-d\TH:i:s\Z", $dateFrom);
        $timeTo = date("Y-m-d\TH:i:s\Z", $dateTo);

        return $this->connector->getRefunds($timeFrom, $timeTo, $offset)['refunds'];
    }

    /**
     * Update offer on allegro
     * @param string $offerId ID of offer which will be updated
     * @param array $data New data for order
     * @return mixed
     * @throws GuzzleException
     */
    public function updateOffer(string $offerId, array $data): mixed
    {
        return $this->connector->updateOffer($offerId, $data);
    }

    /**
     * Return authorization and refresh token
     * @return bool
     * @throws GuzzleException
     */
    public function authorizeOrRefreshToken(): bool
    {
        if ($this->checkAuthorization())
            return true;

        try {
            $this->refreshToken();
            return true;
        } catch (\Exception $e) {
        }

        return false;
    }

    /**
     * Set if you want or not to ignore queue for next requests
     * @param bool $ignoreQueueForRequests
     */
    public function setIgnoreQueueForRequests(bool $ignoreQueueForRequests)
    {
        $this->connector->setIgnoreQueueForRequests($ignoreQueueForRequests);
    }
}
