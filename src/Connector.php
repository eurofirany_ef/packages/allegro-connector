<?php

namespace Eurofirany\AllegroConnector;

use Eurofirany\AllegroConnector\Models\AllegroAccount;
use GuzzleHttp\Exception\GuzzleException;

class Connector
{

    private Api $api;

    const LIMIT = 50;

    private array $parameters;

    /**
     * Connector constructor.
     * @param AllegroAccount $allegroAccount
     */
    public function __construct(AllegroAccount $allegroAccount)
    {
        // Create API
        $this->api = new Api();

        // Set clientId
        $this->api->setClientId($allegroAccount->client_id);

        // Set secret
        $this->api->setSecret($allegroAccount->secret);

        // Set token data
        $this->setTokenData($allegroAccount);
    }

    /**
     * @param $deviceCode
     */
    public function setDeviceCode($deviceCode)
    {
        $this->api->setDeviceCode($deviceCode);
    }

    /**
     * @param $token
     */
    public function setToken($token)
    {
        $this->api->setToken($token);
    }

    /**
     * @param int $page
     * @param int $limit
     * @return mixed
     * @throws GuzzleException
     */
    public function getOffers(int $page = 0, int $limit = 0): mixed
    {
        // Set header
        $this->api->setHeader('Bearer');

        // Set offset
        $this->api->setOffset($this->calculateOffset($page));

        // Set limit
        $this->api->setLimit($limit ? $limit : self::LIMIT);

        // Add additional parameters
        if (count($this->parameters))
            foreach ($this->parameters as $key => $value)
                $this->api->setFormParam($key, $value);

        // Get data
        return $this->api->request('GET', 'sale/offers');
    }

    /**
     * @param int|string $id
     * @param int $ttl
     * @return mixed
     * @throws GuzzleException
     */
    public function getOffer(int|string $id, int $ttl = 3360): mixed
    {
        // Set header
        $this->api->setHeader('Bearer');

        // Get data
        return $this->api->request('GET', 'sale/offers/' . $id, $ttl);
    }

    /**
     * @param string|int $id
     * @return mixed
     * @throws GuzzleException
     */
    public function getProduct(string|int $id): mixed
    {
        // Set header
        $this->api->setHeader('Bearer');

        // Get data
        return $this->api->request('GET', 'sale/products/' . $id);
    }

    /**
     * @return mixed
     * @throws GuzzleException
     */
    public function getMe(): mixed
    {
        // Set header
        $this->api->setHeader('Bearer');

        // Get data
        return $this->api->request('GET', 'me', 0);
    }

    /**
     * @param string|int $setId
     * @return mixed
     * @throws GuzzleException
     */
    public function getVariant(string|int $setId): mixed
    {
        // Set header
        $this->api->setHeader('Bearer');

        // Set accept header
        $this->api->setAccept();

        // Get data
        return $this->api->request('GET', 'sale/offer-variants/' . $setId, 36000);
    }

    /**
     * @param string|int $userId
     * @param int $page
     * @param int $limit
     * @return mixed
     * @throws GuzzleException
     */
    public function getVariants(string|int $userId, int $page = 1, int $limit = 50): mixed
    {
        // Set header
        $this->api->setHeader('Bearer');

        // Set accept header
        $this->api->setAccept();

        // Set offset
        $this->api->setOffset($this->calculateOffset($page));

        // Set user.id
        $this->api->setUserId($userId);

        // Set limit
        $this->api->setLimit(self::LIMIT);

        // Get data
        return $this->api->request('GET', 'sale/offer-variants', 0);
    }

    /**
     * @return mixed
     * @throws GuzzleException
     */
    public function getAccessCodes(): mixed
    {
        // Set header
        $this->api->setHeader('Basic');

        // Set base uri
        $this->api->setBAseUri('auth');

        // Set client id
        $this->api->setFormParam('client_id', $this->api->getClientId());

        // get data
        return $this->api->request('POST', 'auth/oauth/device');
    }

    /**
     * @param string $deviceCode
     * @return mixed
     * @throws GuzzleException
     */
    public function getToken(string $deviceCode): mixed
    {
        // Set header
        $this->api->setHeader('Basic');

        // Set base uri
        $this->api->setBAseUri('auth');

        // Set device code and grant type
        $this->api->setFormParam('grant_type', 'urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Adevice_code');
        $this->api->setFormParam('device_code', $deviceCode);

        // Get data
        return $this->api->request('POST', 'auth/oauth/token');
    }

    /**
     * @param AllegroAccount $allegroAccount
     * @return bool
     */
    public function setTokenData(AllegroAccount $allegroAccount): bool
    {
        if (!isset($allegroAccount->token->token))
            return false;

        // Set token
        $this->api->setToken($allegroAccount->token->token);

        // Set device code
        $this->api->setDeviceCode($allegroAccount->token->device_code);

        return true;
    }

    /**
     * @param int $page
     * @return float|int
     */
    private function calculateOffset(int $page): float|int
    {
        $offset = ((int)$page * (int)self::LIMIT) - (int)self::LIMIT;

        return $offset < 0
            ? 0
            : $offset;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return self::LIMIT;
    }

    /**
     * @param string $name
     * @param array $offers
     * @param string $parameter
     * @param string|null $bindingParameter
     * @return mixed
     * @throws GuzzleException
     */
    public function createVariant(
        string  $name,
        array   $offers,
        string  $parameter,
        ?string $bindingParameter
    ): mixed
    {
        // Set header
        $this->api->setHeader('Bearer');

        // Set accept header
        $this->api->setAccept();

        // Set content type
        $this->api->setContentType();

        // Set params
        $this->api->setBodyJson('name', $name);
        $this->api->setBodyJson('offers', $offers);

        $parameters[] = ['id' => $parameter];

        if ($bindingParameter)
            $parameters[] = ['id' => $bindingParameter];

        $this->api->setBodyJson('parameters', $parameters);

        // Send data
        return $this->api->request('POST', 'sale/offer-variants', 0);
    }

    /**
     * @param string $key
     * @param $value
     */
    public function addParameter(string $key, $value)
    {
        $this->parameters[$key] = $value;
    }

    /**
     * @param $categoryId
     * @return mixed
     * @throws GuzzleException
     */
    public function getCategory($categoryId): mixed
    {
        // Set header
        $this->api->setHeader('Bearer');

        // Set accept header
        $this->api->setAccept();

        // Get data
        return $this->api->request('GET', 'sale/categories/' . $categoryId);
    }


    /**
     * @return mixed
     * @throws GuzzleException
     */
    public function getCategories(): mixed
    {
        // Set header
        $this->api->setHeader('Bearer');

        // Set accept header
        $this->api->setAccept();

        // Get data
        return $this->api->request('GET', 'sale/categories');
    }

    /**
     * Get category available parameters
     * @param $categoryId
     * @return mixed
     * @throws GuzzleException
     */
    public function getCategoryParameters($categoryId): mixed
    {
        // Set header
        $this->api->setHeader('Bearer');

        // Set accept header
        $this->api->setAccept();

        // Get data
        return $this->api->request('GET', 'sale/categories/' . $categoryId . '/parameters');
    }

    /**
     * Remove variant set
     * @param string|int $variantId
     * @throws GuzzleException
     */
    public function removeVariant(string|int $variantId)
    {
        // Set header
        $this->api->setHeader('Bearer');

        // Hit service
        $this->api->request('DELETE', 'sale/offer-variants/' . $variantId);
    }

    /**
     * @param string $refreshToken
     * @return mixed
     * @throws GuzzleException
     */
    public function getRefreshedToken(string $refreshToken): mixed
    {
        // Set header
        $this->api->setHeader('Basic');

        // Set base uri
        $this->api->setBAseUri('auth');

        // Set params
        $this->api->setFormParam('grant_type', 'refresh_token');
        $this->api->setFormParam('refresh_token', $refreshToken);

        // get data
        return $this->api->request('POST', 'auth/oauth/token');
    }

    /**
     * @param string|int $userId
     * @param int $page
     * @param int $limit
     * @return mixed
     * @throws GuzzleException
     */
    public function getSets(string|int $userId, int $page = 0, int $limit = 0): mixed
    {
        // Set header
        $this->api->setHeader('Bearer');

        // Set accept header
        $this->api->setAccept();

        // Set offset
        $this->api->setOffset($this->calculateOffset($page));

        // Set user id
        $this->api->setUserId($userId);

        // Set limit
        $this->api->setLimit($limit ? $limit : self::LIMIT);

        // Add additional parameters
        if (isset($this->parameters) && count($this->parameters))
            foreach ($this->parameters as $key => $value)
                $this->api->setFormParam($key, $value);

        // Get data
        return $this->api->request('GET', 'sale/loyalty/promotions', 0);
    }

    /**
     * @param int|string $setId
     * @return mixed
     * @throws GuzzleException
     */
    public function getSet(int|string $setId): mixed
    {
        // Set header
        $this->api->setHeader('Bearer');

        // Set accept header
        $this->api->setAccept();

        // Get data
        return $this->api->request('GET', 'sale/loyalty/promotions/' . $setId);

    }

    public function removeSet(string|int $setId)
    {
        // Set header
        $this->api->setHeader('Bearer');

        // Hit service
        $this->api->request('DELETE', 'sale/loyalty/promotions/' . $setId);
    }

    /**
     * @param string $offerType
     * @param array $offerIds
     * @param int $discount
     * @throws GuzzleException
     */
    public function generateSets(string $offerType, array $offerIds, int $discount)
    {
        $this->api->setHeader('Bearer');
        $this->api->setAccept();
        $this->api->setContentType();

        $this->api->setBodyJson('benefits', [[
            'specification' => [
                'type' => $offerType,
                'value' => [
                    'amount' => $discount,
                    'currency' => 'PLN'
                ]
            ]]
        ]);

        $this->api->setBodyJson('offerCriteria', [[
            'type' => 'CONTAINS_OFFERS',
            'offers' => $offerIds
        ]]);

        $this->api->request('POST', 'sale/loyalty/promotions', 0);
    }

    /**
     * @param array $params
     * @param string|null $status
     * @param int $offset
     * @return mixed
     * @throws GuzzleException
     */
    public function getOrders(array $params, ?string $status, int $offset): mixed
    {
        $this->api->resetParameters();
        $this->api->setHeader('Bearer');
        $this->api->setAccept();
        $this->api->setContentType();

        $this->api->addQueryParam('offset', $offset);

        if ($status)
            $this->api->addQueryParam('status', $status);

        foreach ($params as $name => $value)
            $this->api->addQueryParam($name, $value);

        return $this->api->request(
            'GET',
            'order/checkout-forms',
            0
        );
    }

    /**
     * @param string $timeFrom
     * @param string $timeTo
     * @param int $offset
     * @return mixed
     * @throws GuzzleException
     */
    public function getRefunds(string $timeFrom, string $timeTo, int $offset = 0): mixed
    {
        $this->api->resetParameters();
        $this->api->setHeader('Bearer');
        $this->api->setAccept();
        $this->api->setContentType();

        $this->api->addQueryParam('occurredAt.gte', $timeFrom);
        $this->api->addQueryParam('occurredAt.let', $timeTo);
        $this->api->addQueryParam('limit', 100);
        $this->api->addQueryParam('offset', $offset);

        return $this->api->request(
            'GET',
            'payments/refunds',
            0
        );
    }

    /**
     * @param string $orderId
     * @param array $data
     * @return mixed
     * @throws GuzzleException
     */
    public function updateOffer(string $orderId, array $data): mixed
    {
        $this->api->resetParameters();
        $this->api->setHeader('Bearer');
        $this->api->setAccept('application/vnd.allegro.public.v1+json');
        $this->api->setContentType('application/vnd.allegro.public.v1+json');

        foreach ($data as $name => $value)
            $this->api->setBodyJson($name, $value);

        return $this->api->request(
            'PATCH',
            sprintf("sale/product-offers/%s", $orderId),
            0
        );
    }

    /**
     * Set if you want or not to ignore queue for next requests
     * @param bool $ignoreQueueForRequests
     */
    public function setIgnoreQueueForRequests(bool $ignoreQueueForRequests)
    {
        $this->api->setIgnoreQueueForRequests($ignoreQueueForRequests);
    }
}
